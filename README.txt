This module provides various functionality around the status of modules on a site.
It is highly customisable and can be used for different purposes in different environments (production, stage, development).

<strong>You usually don't want to have the same modules enabled in production, stage and development.
This module let's you define what modules should (or should not) be enabled (or disabled) per environment.</strong>

<strong>In a production environment </strong>you may want to only allow users to change the status of a specified list of modules rather than all of them.
For example only allowing users to enable/disable modules like dblog, devel, field_ui, views_ui etc and if you forget to disable those modules yourself, they will eventually be disabled by a cron job.

<strong>In a development environment</strong> you may want to automatically have some "production only" modules disabled and have some development modules enabled etc.

<strong>Example configuration:</strong>

// List of modules that can be (temporarily) enabled or disabled.
$conf['module_status_modules_modifiable'] = array(
  'dblog',
  'devel',
  'field_ui',
  'views_ui',
);

// List of modules that should be disabled.
// Should also be listed in "module_status_modules_modifiable".
$conf['module_status_modules_disable'] = array(
  'devel',
  'field_ui',
  'views_ui'
);

// List of modules that should be enabled.
// Should also be listed in "module_status_modules_modifiable".
$conf['module_status_modules_enable'] = array(
  'dblog',
);