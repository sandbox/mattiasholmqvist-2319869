<?php

/**
 * @file
 * Example configuration of module_status.
 *
 * It is recommended to have a production.inc file included in production, a
 * stage.inc file for stage and a development.inc file for development.
 *
 * Alternatively you can put the configuration for each environment directly in
 * settings.php.
 */

// List of modules that can be (temporarily) enabled or disabled.
$conf['module_status_modules_modifiable'] = array(
  'dblog',
  'devel',
  'field_ui',
  'views_ui',
);

// List of modules that should be disabled.
// Should also be listed in "module_status_modules_modifiable".
$conf['module_status_modules_disable'] = array(
  'devel',
  'field_ui',
  'views_ui'
);

// List of modules that should be enabled.
// Should also be listed in "module_status_modules_modifiable".
$conf['module_status_modules_enable'] = array(
  'dblog',
);